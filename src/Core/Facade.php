<?php 

namespace Acme\Core;

class Facade {

      public static function __callStatic($method, $args)
      {
            $container = new Container;
            $instance = $container->make(static::getAccessor());

            return call_user_func_array(array($instance, $method), $args);
      }
}