<?php 

namespace Acme\Core;

class Container {

      protected $bindings = array(
            'desert' => '\Acme\Food\DessertClass',
            'lunch'  => '\Acme\Food\LunchClass'
      );

      public function make($name)
      {
            return new $this->bindings[$name]();
      }
}