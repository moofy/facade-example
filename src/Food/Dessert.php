<?php namespace Acme\Food;

class Dessert extends \Acme\Core\Facade {

      public static function getAccessor()
      {
            return 'desert';
      }
}