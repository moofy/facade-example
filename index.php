<?php

include('vendor/autoload.php');

\Acme\Core\Alias::load([
	'Dessert' => '\Acme\Food\Dessert',
	'Lunch' => '\Acme\Food\Lunch'
]);

echo 'For lunch: ' . Lunch::getFood() . '<br>';
echo 'For dessert: ' . Dessert::getFood();
